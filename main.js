/** Rain Game 1: reorganized code **/

window.onload = myGame;
enchant();

function myGame() {
    // New game with a 320x320 pixel canavs, 24 frames/sec.
    var game = new Core(320, 320);
    game.fps = 24;

    // Preload assets.
    game.preload('droplet24.png');
    game.preload('usertrash32.png');
    game.preload('sky.png');

    // Specify what should happen when the game loads.
    game.onload = function () {
        // Background image.
        var bg = new Sprite(game.width, game.height);
        bg.image = game.assets['sky.png'];
        game.rootScene.addChild(bg);
        
        // Initial gameplay parameters
        var numDroplets = 4;    // How many droplets in game?
        
        // Make the collector
        var collector = makeCollector();
        
        // Create a bunch autonomous droplets.
        // (Droplets are not stored in an array because access will not
        // be needed in this game.)
        for (var i = 0; i < numDroplets; i += 1) {
            makeDroplet(collector);
        }
    };
    
    // Start the game.
    game.start();
    
    // ==== myGame helper functions ====================================
    /** Make a droplet with the needed listeners. If droplet collides
     * with collector, remove droplet from scene.
     * Return a reference to the created droplet. */
    function makeDroplet(collector) {
        var droplet = new Sprite(24, 24);
        droplet.image = game.assets['droplet24.png'];
        
        // Add a gravity property to the droplet.
        droplet.gravity = 5;    // How fast will the droplet drop?
        
        // Position droplet in random x and random y above screen.
        droplet.x = randomInt(0, game.width - droplet.width);
        droplet.y = randomInt(-1 * game.height, -1 * droplet.height);
        
        // Add droplet to scene.
        game.rootScene.addChild(droplet);
        
        // Add an event listener to the droplet Sprite to move it.
        droplet.addEventListener(Event.ENTER_FRAME, function () {
            // Move.
            if (droplet.y > game.height) {   // if below canvas, reset.
                droplet.x = randomInt(0, game.width - droplet.width);
                droplet.y = -1 * droplet.height;
            } else {                         // else move down as usual.
                droplet.y += droplet.gravity;
            }
            
            // Check for collision.
            if (droplet.intersect(collector)) {
                game.rootScene.removeChild(droplet);
            }
        });
        
        return droplet;
    }
    
    /** Make a collector with the needed listeners.
     * Return a reference to the created object. */ 
    function makeCollector() {
        var collector = new Sprite(32, 32);
        collector.image = game.assets['usertrash32.png'];

        // Add a speed property to the collector.
        collector.speed = 4;  // How fast will collector move?

        collector.x = (game.width + collector.width) / 2;
        collector.y = game.height - collector.height;
        
        // Add collector to scene.
        game.rootScene.addChild(collector);

        // Event listeners for collector.
        collector.addEventListener(Event.ENTER_FRAME, function () {
            // Move.
            if (game.input.right && !game.input.left) {
                collector.x += collector.speed;
            } else if (game.input.left && !game.input.right) {
                collector.x -= collector.speed;
            }
            
            // Check limits.
            if (collector.x > game.width - collector.width) {
                collector.x = game.width - collector.width;
            } else if (collector.x < 0) {
                collector.x = 0;
            }
        });
        
        return collector;
    }
    
    /** Generate a random integer between low and high (inclusive). */
    function randomInt(low, high) {
        return low + Math.floor((high + 1 - low) * Math.random());
    }
    // ==== End myGame helper functions ================================
}
